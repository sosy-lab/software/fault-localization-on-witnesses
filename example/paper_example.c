extern int __VERIFIER_nondet_int();
extern void reach_error();

void main() {
  int num = __VERIFIER_nondet_int();
  if (num < 1) return;

  for (int i=2; i <= num; i++) {
    if (num % i != 0) {
      continue;
    }
    int isPrime = 1;
    for (int j=2; j <= num/2; j++) {
      if (num % j == 0) {
        isPrime = 0;
        break;
      }
    }
    if (isPrime) {
      num = num / (i + 1);
      i--;
    }
  }

  if(num != 1) {
    reach_error();
  }
}
