#!/bin/python3

import json
import argparse
import glob
import logging
import os
from enum import Enum
from multiprocessing import Pool
import subprocess

import networkx as nx
from dataclasses import dataclass
from typing import List, Tuple
from pathlib import Path
from os.path import isfile


class DeleteMode(Enum):
    NEVER = 1
    ALWAYS = 2
    ASSUMPTION = 3
    ASSUMPTION_PLUS = 4


@dataclass
class Location:
    start_line: int
    end_line: int
    start_offset: int
    end_offset: int

    @staticmethod
    def from_location_json(loc_json):
        return Location(
            int(loc_json["startLine"]),
            int(loc_json["endLine"]),
            int(loc_json["startOffset"]),
            int(loc_json["endOffset"]),
        )


def locations_to_delete(fault_localization: str) -> List[List[Location]]:
    with open(fault_localization, "r") as fl:
        fl_dict = json.load(fl)
        faults = fl_dict["faults"]
        all_locs = []
        for fault in faults:
            locs = []
            for contrib in fault["fault"]["contributions"]:
                location = contrib["fault-contribution"]["location"]
                locs.append(Location.from_location_json(location))
            all_locs.append(locs)
        return all_locs


def filter_unique(filter_list, iterable):
    curr = list(iterable)
    for filt in filter_list:
        curr = list(filter(filt, curr))
        if len(curr) == 1:
            return curr[0]
        if not curr:
            return None
    return None


def is_location_described_by(location: Location, locations: List[Location]) -> bool:
    for loc in locations:
        if loc.start_line != location.start_line:
            continue
        if location.end_line:
            if loc.end_line != location.end_line:
                continue
        if location.start_offset:
            if loc.start_offset != location.start_offset:
                continue
        if location.end_offset:
            if loc.end_offset != location.end_offset:
                continue
        return True
    return False


def collected_data_to_csv(data, out, columns):
    import pandas as pd

    # create DataFrame using data
    df = pd.DataFrame(data, columns=columns)
    df.to_csv(out, index=False)


def detect_trivial_witness(graphml):
    try:
        if len(graphml.edges()) == 0:
            return True
        if len(graphml.edges()) == 1:
            _, _, data = list(graphml.edges(data=True))[0]
            for key in data.keys():
                if "assumption" in key:
                    return False
            return True
        return False
    except ValueError:
        return None


def modify_witness(
    witness_xml,
    locations: List[Location],
    output="witness.graphml",
    write_to_out=True,
    delete_mode=DeleteMode.NEVER,
) -> Tuple[int, int, int, int, int, int, int, int]:
    def smart_convert(something):
        if something is None:
            return None
        return int(something)

    graphml = nx.read_graphml(witness_xml)
    is_trivial = detect_trivial_witness(graphml)
    nodes_before, edges_before = len(graphml.nodes()), len(graphml.edges())
    delete_edges = []
    deleted_assumptions = 0
    entry_nodes = list(
        filter(
            lambda n: n[1].get("entry", "false") == "true"
            or n[1].get("entry", "false") is True
            or n[1].get("isEntryNode", False),
            graphml.nodes(data=True),
        )
    )
    assert len(entry_nodes) == 1, (entry_nodes, witness_xml)
    entry_node = entry_nodes[0][0]
    for n1, n2, data in graphml.edges(data=True):
        start_line = data.get("startline", None)
        if not start_line:
            continue
        end_line = data.get("endline", None)
        start_offset = data.get("startoffset", None)
        end_offset = data.get("endoffset", None)
        loc = Location(
            smart_convert(start_line),
            smart_convert(end_line),
            smart_convert(start_offset),
            smart_convert(end_offset),
        )
        start_lines = set(l.start_line for l in locations)
        if not is_location_described_by(loc, locations) and start_lines:
            has_assumption = False
            for key in [a for a in data.keys() if "assumption" in a]:
                data.pop(key)
                has_assumption = True
            if has_assumption:
                deleted_assumptions += 1
            if has_assumption and delete_mode == DeleteMode.ASSUMPTION_PLUS:
                delete_edges.append((n1, n2, data))
            elif (
                has_assumption and delete_mode == DeleteMode.ASSUMPTION
            ) or delete_mode == DeleteMode.ALWAYS:
                allowed_data_keys = {
                    "startline",
                    "endline",
                    "startoffset",
                    "endoffset",
                    "originfile",
                    "originFileName",
                    "sourcecode",
                    "violatedProperty",
                    "witness-type",
                    "threadId",
                    "stmt",
                    "cSource",
                    "createThread",
                    "violatedProperty",
                    "sourcecodeLanguage",
                    "sourcecodelang",
                    "programFile",
                    "programfile",
                    "programHash",
                    "programhash",
                    "specification",
                    "architecture",
                    "producer",
                    "id",
                }
                if not set(data.keys()).difference(allowed_data_keys):
                    if n1 != entry_node:
                        delete_edges.append((n1, n2, data))
    for pred, succ, data in delete_edges:
        if len(graphml.pred[succ]) <= 1 and len(graphml.succ[pred]) <= 1:
            for s, d in graphml.pred[pred].items():
                if 0 in d.keys():
                    d = d[0]
                graphml.add_edge(s, succ, **d)
            graphml.remove_edge(pred, succ)
            graphml.remove_node(pred)
    nodes_after = len(graphml.nodes())
    edges_after = len(graphml.edges())
    assert nodes_before - nodes_after >= 0 and edges_before - edges_after >= 0
    target_path = Path(output)
    target_path.parent.mkdir(parents=True, exist_ok=True)
    if output.endswith(".bz2"):
        output = output[:-3]
    nx.write_graphml(
        graphml,
        output,
        named_key_ids=True,
    )
    replacements = {
        "isEntryNode": "entry",
        "isSinkNode": "sink",
        "isViolationNode": "violation",
        "enterLoopHead": "enterLoopHead",
        "violatedProperty": "violatedProperty",
        "sourcecodeLanguage": "sourcecodelang",
        "programFile": "programfile",
        "programHash": "programhash",
        "specification": "specification",
        "architecture": "architecture",
        "producer": "producer",
        "creationTime": "creationtime",
        "startline": "startline",
        "endline": "endline",
        "startoffset": "startoffset",
        "endoffset": "endoffset",
        "originFileName": "originfile",
        "control": "control",
        "assumption": "assumption",
        "assumption.scope": "assumption.scope",
        "enterFunction": "enterFunction",
        "returnFromFunction": "returnFrom",
        "witness-type": "witness-type",
        "inputWitnessHash": "inputwitnesshash",
    }
    with open(output, "r") as fp:
        text = fp.read()
        for k, v in replacements.items():
            text = text.replace(f'id="{k}"', f'id="{v}"')
            text = text.replace(f'key="{k}"', f'key="{v}"')
            text = text.replace(">True<", ">true<")
            text = text.replace(">False<", ">false<")

    if write_to_out:
        with open(output, "w") as fp:
            fp.write(text)
    return (
        nodes_before,
        edges_before,
        nodes_after,
        edges_after,
        nodes_before - nodes_after,
        edges_before - edges_after,
        deleted_assumptions,
        is_trivial,
    )


def modify_witnesses(
    witness_dir, fl_dir, output_dir, selection=lambda l: l, delete_mode=DeleteMode.NEVER
):
    # expects log/rundef/task-def/witness.graphml
    witnesses = glob.glob(
        f"{witness_dir}/SV-COMP23_unreach-call/**/witness.graphml.bz2"
    )
    tool_name = os.path.basename(witness_dir).split(".")[0]
    count = 0
    num_witnesses = len(witnesses)
    collected_data = []
    logging.info(f"Consider {witness_dir} with {num_witnesses} witnesses")
    for witness in witnesses:
        fl = witness.replace(witness_dir, fl_dir)[: -len("/witness.graphml.bz2")]
        fl = f"{fl}/output/faultlocalization.json"
        if not isfile(fl):
            continue
        out = witness.replace(witness_dir, output_dir).replace("//", "/")
        faults = selection(list(locations_to_delete(fl)))
        if faults:
            collected_data.append(
                (
                    out,
                    witness,
                    fl,
                    *modify_witness(
                        witness, faults[0], output=out, delete_mode=delete_mode
                    ),
                )
            )
            count += 1
    logging.info(f"Modified {count}/{len(witnesses)} from {witness_dir}")
    return collected_data, (witness_dir, fl_dir, tool_name, num_witnesses, count)


def wrap_modify_witnesses(t):
    return modify_witnesses(*t)


def sort_ascending(elements):
    return sorted(elements, key=len)


def sort_descending(elements):
    return sorted(elements, key=len, reverse=True)


def find_pairs(
    results_verified,
    test_results,
    output_root,
    debug=False,
    delete_mode=DeleteMode.NEVER,
):
    runs = []
    for witness_dir in glob.glob(f"{results_verified}/**.files"):
        prefix = os.path.basename(witness_dir).split(".")[0]
        for json_dir in glob.glob(f"{test_results}/{prefix.lower()}-**.files"):
            for strategy_name, strategy in [
                ("min", sort_ascending),
                ("max", sort_descending),
            ]:
                if not os.path.isdir(json_dir):
                    logging.warning(f"Skip not a JSON directory {json_dir}")
                output_root = (
                    output_root[:-1] if output_root[-1] == "/" else output_root
                )
                output = (
                    f"{output_root}/{prefix}/{strategy_name}-"
                    f'{json_dir.split(f"{prefix.lower()}-", maxsplit=1)[1].split(".")[0]}'
                )
                runs.append((witness_dir, json_dir, output, strategy, delete_mode))
    data = []
    dir_data = []
    if debug:
        for run in runs:
            partial_data, partial_dir_data = modify_witnesses(*run)
            data += partial_data
            dir_data.append(partial_dir_data)
    else:
        with Pool(os.cpu_count()) as p:
            all_data = p.map(wrap_modify_witnesses, runs)

        for partial_data, partial_dir_data in all_data:
            data += partial_data
            dir_data.append(partial_dir_data)

    collected_data_to_csv(
        data,
        "output/data.csv",
        [
            "OutName",
            "GraphML",
            "JSON",
            "NodesBefore",
            "EdgesBefore",
            "NodesAfter",
            "EdgesAfter",
            "NodesDeleted",
            "EdgesDeleted",
            "AssumptionsDeleted",
            "IsTrivial",
        ],
    )
    collected_data_to_csv(
        dir_data,
        "output/dir_data.csv",
        ["WitnessRoot", "FLRoot", "ToolName", "NumberWitnesses", "NumberModified"],
    )


def run_cpachecker(
    program: str,
    witness: str,
    delete_mode: str,
    out: str,
    algorithm: str = "MaxSat",
    spec: str = str(
        Path(__file__).parent.absolute()
        / "../cpachecker/config/properties/unreach-call.prp"
    ),
    bits: int = 32,
):
    algorithm = {"MaxSat": "MAXORG", "MinUnsat": "MAXSAT", "Unsat": "UNSAT"}[algorithm]
    program = Path(program).absolute()
    witness = Path(witness).absolute()
    cmd = f"""scripts/cpa.sh -violation-witness-validation -setprop witness.checkProgramHash=false -heap 5000m -disable-java-assertions 
-setprop cpa.predicate.memoryAllocationsAlwaysSucceed=true 
-setprop cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc 
-setprop cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc 
-setprop cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc 
-setprop cpa.smg.deallocationFunctions=free,kfree,kfree_const 
-setprop analysis.alwaysStoreCounterexamples=true 
-setprop counterexample.export.exportFaults=true 
-setprop faultLocalization.export.outputFile=faultlocalization.json 
-setprop analysis.algorithm.faultLocalization.by_traceformula=true 
-setprop faultLocalization.by_traceformula.requirePreciseCounterexample=false 
-setprop traceformula.inlinePrecondition=false 
-setprop faultLocalization.by_traceformula.type={algorithm} 
-setprop faultLocalization.by_traceformula.stopAfterFirstFault=false 
-setprop faultLocalization.by_traceformula.preconditionType=NONDETERMINISTIC_VARIABLES_ONLY 
-witness {witness} 
-timelimit 900s
-spec {spec} 
-{bits} 
{program}
""".replace(
        "\n", " "
    )
    print(cmd)
    process = subprocess.Popen(
        cmd,
        shell=True,
        cwd=Path(__file__).parent.resolve() / Path("../cpachecker"),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = process.communicate()
    stdout = stdout.decode("utf-8")
    stderr = stderr.decode("utf-8")
    logging.info(stdout)
    logging.warning(stderr)
    if any(
        [out_line.startswith("exception") for out_line in stdout.lower().split("\n")]
    ) or any(
        [err_line.startswith("exception") for err_line in stderr.lower().split("\n")]
    ):
        raise Exception(f"Console:\n{stdout}\n\nError:\n{stderr}")
    modify_witness(
        witness,
        sort_ascending(
            locations_to_delete(
                str(
                    Path(__file__).parent.resolve()
                    / "../cpachecker/output/faultlocalization.json"
                )
            )
        )[0],
        output=out,
        delete_mode=delete_mode,
    )
    logging.info(f"The reduced/collapsed witness can be found here: {out}")
    print(delete_mode)


def main(argv=None):
    parser = argparse.ArgumentParser(
        description="Apply Fault Localization to Witnesses."
    )
    parser.add_argument("--witness", "-w", help="graphml file")
    parser.add_argument("--fl", "-f", help="fault localization json file")
    parser.add_argument(
        "--delete-mode",
        "-dm",
        default="never",
        help="assumption: delete an edge if assumption is removed and edge does not contain any control flow data, "
        "never: do not delete edges, "
        "always: delete edges if the do not contain control flow data (before or after removing assumptions)",
    )
    parser.add_argument(
        "--output",
        "-o",
        help="output file",
        default=str(Path(__file__).parent.absolute() / "../out/flow.graphml"),
    )
    parser.add_argument("--witness-directory", "-g", help="graphml file")
    parser.add_argument("--fl-directory", "-j", help="fault localization json file")
    parser.add_argument("--output-directory", "-d", help="output directory")
    parser.add_argument(
        "--algorithm",
        "-a",
        help="which fl algorithm to use",
        choices=["MaxSat", "MinUnsat", "Unsat"],
        default="MaxSat",
    )
    parser.add_argument("--program", "-p", help="C program")
    parser.add_argument(
        "--specification",
        "-spec",
        help="specification in CPAchecker project",
        default=str(
            Path(__file__).parent.absolute()
            / "../cpachecker/config/properties/unreach-call.prp"
        ),
    )
    parser.add_argument(
        "--data-model", "-m", help="machine model", default="32", choices=["32", "64"]
    )
    parser.add_argument(
        "--folders-are-superfolders",
        "-s",
        action="store_true",
        help="Whether -g, -j are folders containing a bunch of folders.",
    )
    opts = parser.parse_args(argv)
    delete_mode = {
        "assumption": DeleteMode.ASSUMPTION,
        "never": DeleteMode.NEVER,
        "always": DeleteMode.ALWAYS,
        "assumptionpp": DeleteMode.ASSUMPTION_PLUS,
    }[opts.delete_mode]
    if opts.program and opts.witness and delete_mode:
        logging.info("Run CPAchecker...")
        run_cpachecker(
            opts.program,
            opts.witness,
            delete_mode,
            opts.output,
            algorithm=opts.algorithm,
            spec=opts.specification,
            bits=int(opts.data_model),
        )
    elif (
        opts.fl
        and opts.witness
        and opts.output
        and not opts.fl_directory
        and not opts.witness_directory
        and not opts.output_directory
        and not opts.folders_are_superfolders
    ):
        logging.info(
            modify_witness(
                opts.witness,
                locations_to_delete(opts.fl)[0],
                output=opts.output,
                delete_mode=delete_mode,
            )
        )
    elif (
        not opts.fl
        and not opts.witness
        and not opts.output
        and opts.fl_directory
        and opts.witness_directory
        and opts.output_directory
    ):
        if not opts.folders_are_superfolders:
            modify_witnesses(
                opts.witness_directory, opts.fl_directory, opts.output_directory
            )
        else:
            find_pairs(
                opts.witness_directory,
                opts.fl_directory,
                opts.output_directory,
                delete_mode=delete_mode,
            )
    else:
        raise Exception(
            "Too many arguments specified: Specify either fl, witnesses, output "
            "XOR witness-directory, fl-directory, output. Do not mix them."
        )


if __name__ == "__main__":
    main()
