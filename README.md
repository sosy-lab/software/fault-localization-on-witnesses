# Fault Localization on Witnesses

## Install

CPAchecker requires Java 17.
The scripts require Python 3.10.
Install the required packages for python with `pip3 install -r requirements.txt`.
Install `ant` to build CPAchecker with `sudo apt install ant`.
Finally, run `setup.sh` from the project directory to install CPAchecker.

## Example

Run `./flow --program example/paper_example.c --witness example/example.graphml -delete-mode always`
to reduce the witness in `example/example.graphml` with CPAchecker to a smaller witness
eventually found in `out/flow.graphml.`

